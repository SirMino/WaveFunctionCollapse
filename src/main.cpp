//#include <cstdlib>
#include <iostream>
#include <fstream>
//#include <iterator>
#include <stdio.h>
#include <string.h>
#include <string>
#include <vector>
//#include <list>
//#include <memory>
#include <algorithm>
#include <cmath>
#ifdef _WIN32
    #include <ctime>
    #include "../include/raylib.h"
    #define strtok_r strtok_s
#elif __linux__
    #include <raylib.h>
#endif

const int TILE_SIZE = 16;
const int SIZE = 16;
const int SIZE_POWER = SIZE*SIZE;
const int SCREEN_SIDE = TILE_SIZE * SIZE;
int RENDER_SIDE = 1024; // SCREEN_SIDE * 10;
const Color PALETTE_BLUE = {51,44,80,255};
const Color PALETTE_CYAN = {70,135,143,255};
const Color PALETTE_GREEN = {148,227,68,255};
const Color PALETTE_WHITE = {226,243,228,255};
int TILES = 0;

struct tile {
    int id;
    int up,right,down,left;
    int ogx, ogy;
};

struct cell {
    int id;
    tile celltile;
    bool collapsed = false;
    std::vector<tile> compatible_cells;
    //std::list<tile> compat_cells;

    bool compare_ids(int _id) {
        return id = _id;
    }

    void remove_tile(int _id) {
        std::vector<tile>::iterator it;
        for (it = compatible_cells.begin(); it != compatible_cells.end();) {
            if (it->id == _id) {
                it = compatible_cells.erase(it);
            } else {
                ++it;
            }
        }
    }
};

std::vector<tile> tiles;
std::vector<cell> grid(SIZE_POWER);
//cell _grid[SIZE][SIZE];

void _print(std::string message) {
    std::cout << message << std::endl;
}

bool _is_resolved() {
    bool resolved = true;
    for (cell _cell : grid) {
        if (!_cell.collapsed)
            resolved = false;
    }
    return resolved;
}

// https://www.udacity.com/blog/2021/05/how-to-read-from-a-file-in-cpp.html
void _open_file() {
    std::fstream file;
    file.open("res/tiles2.cfg", std::ios::in);
    if (file.is_open()) {
        std::string file_line;
        const char *token;
        char* next_token;
        tiles.clear();
        while (file.good()) {
            std::getline(file, file_line);
            token = strtok_r(const_cast<char *>(file_line.c_str()), ";", &next_token);
            int i = 0;
            tile _tile;
            while (token != NULL) {
                switch (i) {
                    case 0: // ID
                        //sscanf_s(token, "%d", &t.id);
                        sscanf(token, "%d", &_tile.id);
                        break;
                    case 1: // og x
                        sscanf(token, "%d", &_tile.ogx);
                        break;
                    case 2: // og y
                        sscanf(token, "%d", &_tile.ogy);
                        break;
                    case 3: // up
                        //sscanf_s(token, "%d", &t.u);
                        sscanf(token, "%d", &_tile.up);
                        break;
                    case 4: // right
                        //sscanf_s(token, "%d", &t.r);
                        sscanf(token, "%d", &_tile.right);
                        break;
                    case 5: // down
                        //sscanf_s(token, "%d", &t.d);
                        sscanf(token, "%d", &_tile.down);
                        break;
                    case 6: // left
                        //sscanf_s(token, "%d", &t.l);
                        sscanf(token, "%d", &_tile.left);
                        break;
                }

                token = strtok_r(NULL, ";", &next_token);
                i++;
            }
            
            //t.texture = LoadTexture(TextFormat("res/%s", t.get_filename().c_str()));
            tiles.push_back(_tile);
            TILES++;
        }
        
        file.close();

        /*
        for (tile _t : tiles) {
            _print(TextFormat("tile %d [x:%d,y:%d] [U:%d,R:%d,D:%d,L:%d]", _t.id, _t.ogx, _t.ogy, _t.up, _t.right, _t.down, _t.left));
        }
        */
    }
}

void _init_grid() {
    for (int i = 0; i < SIZE; i++) {       // rows
        for (int j = 0; j < SIZE; j++) {    // columns
            int _index = j + i * SIZE;
            grid.at(_index).id = _index;
            grid.at(_index).collapsed = false;
        }
    }
}

void update_entropy() {
    for (int i = 0; i < SIZE; i++) {       // rows
        for (int j = 0; j < SIZE; j++) {   // columns

            int _index = j + i * SIZE;
            cell _cell = grid.at(_index);

            // all tiles
            _cell.compatible_cells.clear();
            _cell.compatible_cells.assign(tiles.begin(), tiles.end());
            std::vector<tile> _tiles_to_remove;

            if (!_cell.collapsed) {

                int _offset_index;
                for(int _tile = 0; _tile < _cell.compatible_cells.size(); _tile++) {

                    // look up
                    if (i > 0) {
                        _offset_index = j + (i - 1) * SIZE;
                        if (grid.at(_offset_index).collapsed) {
                            if (grid.at(_offset_index).celltile.down != _cell.compatible_cells[_tile].up) {
                            //    _print(TextFormat("[LOOK UP] [c %d, offset %d] remove tile %d", _index, _offset_index, _cell.compatible_cells[_tile].id));
                                _tiles_to_remove.push_back(_cell.compatible_cells.at(_tile));
                            } // else {
                            //    _print(TextFormat("[LOOK UP] [c %d, offset %d] tile %d ok", _index, _offset_index, _cell.compatible_cells[_tile].id));
                            //}
                            
                        }
                    }
                    // look right
                    if (j < SIZE-1) {
                        _offset_index = j + 1 + i * SIZE;
                        if (grid.at(_offset_index).collapsed) {
                            if (grid.at(_offset_index).celltile.left != _cell.compatible_cells[_tile].right) {
                            //    _print(TextFormat("[LOOK RIGHT] [c %d, offset %d] remove tile %d", _index, _offset_index, _cell.compatible_cells[_tile].id));
                                _tiles_to_remove.push_back(_cell.compatible_cells.at(_tile));
                            } //else {
                            //    _print(TextFormat("[LOOK UP] [c %d, offset %d] tile %d ok", _index, _offset_index, _cell.compatible_cells[_tile].id));
                            //}
                        }
                    }
                    // look down
                    if (i < SIZE-1) {
                        _offset_index = j + (i + 1) * SIZE;
                        if (grid.at(_offset_index).collapsed) {
                            if (grid.at(_offset_index).celltile.up != _cell.compatible_cells[_tile].down) {
                            //    _print(TextFormat("[LOOK DOWN] [c %d, offset %d] remove tile %d", _index, _offset_index, _cell.compatible_cells[_tile].id));
                                _tiles_to_remove.push_back(_cell.compatible_cells.at(_tile));
                            } //else {
                            //    _print(TextFormat("[LOOK UP] [c %d, offset %d] tile %d ok", _index, _offset_index, _cell.compatible_cells[_tile].id));
                            //}
                        }
                    }

                    // look left
                    if (j > 0) {
                        _offset_index = j - 1 + i * SIZE;
                        if (grid.at(_offset_index).collapsed) {
                            if (grid.at(_offset_index).celltile.right != _cell.compatible_cells[_tile].left) {
                            //    _print(TextFormat("[LOOK LEFT] [c %d, offset %d] remove tile %d", _index, _offset_index, _cell.compatible_cells[_tile].id));
                                _tiles_to_remove.push_back(_cell.compatible_cells.at(_tile));
                            } //else {
                            //    _print(TextFormat("[LOOK UP] [c %d, offset %d] tile %d ok", _index, _offset_index, _cell.compatible_cells[_tile].id));
                            //}
                        }
                    }
                }
            }

            // Remove incompatible cells
            for (tile _t : _tiles_to_remove)
                _cell.remove_tile(_t.id);

            grid.at(_index) = _cell;

            /*
            std::string comp_cells;
            for (tile _t : grid.at(_index).compatible_cells) {
                comp_cells = comp_cells.empty() ? TextFormat("%d", _t.id) : TextFormat("%s;%d", comp_cells.c_str(), _t.id);
            }

            _print(TextFormat(
                "cell %d [%s] comp cells: [%s]", 
                _index, 
                grid.at(_index).collapsed ? "C" : "NC",
                // grid.at(_index).compatible_cells.size()));
                comp_cells.c_str()));
            */
        }
    }
}

int get_next_cell() {

    int to_sort[SIZE*SIZE];
    int num = 0;
    int _min = TILES;
    for (cell c : grid) {
        if (!c.collapsed) {
            //to_sort[num] = c.get_entropy();
            if (c.compatible_cells.size() < _min) {
                _min = c.compatible_cells.size();
            }
        }
        num++;
    }

    std::vector<cell> cells;
    std::copy_if(
        grid.begin(), 
        grid.end(), 
        std::back_inserter(cells), 
        [_min](cell c) {
        return !c.collapsed && c.compatible_cells.size() == _min;
    });

    SetRandomSeed((unsigned int) time(0));
    int _rnd_cell = GetRandomValue(0, cells.size() - 1);
    int rnd = cells.at(_rnd_cell).id;
    return rnd;
}

void resolve() {

    // aggiorno entropia
    update_entropy();

    // scelgo tra le celle a minore entropia una delle sue possibili tile
    int _index = get_next_cell();
    //_print(TextFormat("resolve collasso cella %d", _index));
    SetRandomSeed((unsigned int) time(0));
    grid.at(_index).collapsed = true;
    grid.at(_index).celltile = grid.at(_index).compatible_cells[GetRandomValue(0, grid.at(_index).compatible_cells.size() - 1)];
    
}

int main() {

    _open_file();

    _init_grid();

    // InitWindow(screen_render_width, screen_render_height, "Wave Function Collapse c++");
    InitWindow(0, 0, "Wave Function Collapse c++");
    SetTargetFPS(30);

    RENDER_SIDE = SCREEN_SIDE * (GetMonitorHeight(GetCurrentMonitor()) / SCREEN_SIDE - 1);

    const int screen_width = SCREEN_SIDE;
    const int screen_height = SCREEN_SIDE;
    const int screen_render_width = RENDER_SIDE;
    const int screen_render_height = RENDER_SIDE;

    SetWindowSize(RENDER_SIDE, RENDER_SIDE);
    SetWindowPosition((GetMonitorWidth(GetCurrentMonitor()) - RENDER_SIDE) / 2, (GetMonitorHeight(GetCurrentMonitor()) - RENDER_SIDE) / 2);

    const char * _restart_message = "Push R to restart";
    const int _restart_message_lenght = MeasureText(_restart_message, 8);
    bool _show_restart_message = false;

    Texture2D tiles_texture = LoadTexture("res/tiles.png");

    RenderTexture2D render_screen = LoadRenderTexture(screen_width, screen_height);
	while (!WindowShouldClose()) {

        /*
        if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
            resolve();
        }
        */
        if (IsKeyPressed(KEY_R) && _is_resolved()) {
            _init_grid();
        }

        if (!_is_resolved()) {
            resolve();
            _show_restart_message = false;
        } else {
            _show_restart_message = true;
        }

        BeginTextureMode(render_screen);
            ClearBackground(PALETTE_CYAN);

            for (int i = 0; i < SIZE; i++) {       // rows
                for (int j = 0; j < SIZE; j++) {
                    int _index = j + i * SIZE;
                    cell _cell = grid.at(_index);
                    Color _text_color;
                    if (_cell.collapsed) {
                        DrawTextureRec(tiles_texture,
                            Rectangle {(float) _cell.celltile.ogx,(float) _cell.celltile.ogy,TILE_SIZE,TILE_SIZE},
                            Vector2 { (float) j * TILE_SIZE, (float) i * TILE_SIZE},
                            WHITE);
                        _text_color = PALETTE_GREEN;
                    } else {
                        DrawRectangle(j * TILE_SIZE, i * TILE_SIZE, TILE_SIZE, TILE_SIZE, PALETTE_BLUE);
                        _text_color = DARKGRAY;
                    }
                    //DrawText(TextFormat("%d", _cell.id), j * SIZE_POWER + 4, i * SIZE_POWER + 4, 8, _text_color);
                }
            }

            if (_show_restart_message){
                float _alpha = 2 * sin((GetTime() * 100.0f) / 25);
                DrawRectangle((screen_width - _restart_message_lenght) / 2 - 2, screen_height - 12 - 2, _restart_message_lenght + 4, 12, ColorAlpha(PALETTE_BLUE, _alpha));
                DrawText(_restart_message, (screen_width - _restart_message_lenght) / 2, screen_height - 12, 8, ColorAlpha(PALETTE_GREEN, _alpha));
            }

        EndTextureMode();

        BeginDrawing();
            ClearBackground(BLUE);
            DrawTexturePro(render_screen.texture, 
                    Rectangle {0,0,(float)render_screen.texture.width, (float)-render_screen.texture.height},
                    Rectangle {0,0, (float) screen_render_width, (float) screen_render_height},
                    Vector2 {0,0},
                    0.0,
                    WHITE);

        EndDrawing();
    }

    CloseWindow();

    return 0;
}
